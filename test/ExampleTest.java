import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExampleTest {

  public static SQLRequest sqlRequest;

  @Mock
  private ResultSet resultSet;

  @BeforeClass
  public static void before() throws SQLException {
    sqlRequest = new SQLRequest();
  }


  @Test
  public void test() throws SQLException {
    ResultSet resultSet = sqlRequest.executeQuery("SELECT * from product");
    assertEquals(resultSet.next(), true);
    assertEquals(resultSet.getString(2), "House");
    resultSet.close();

  }

  @Test
  public void testConverter() throws SQLException {

    when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
    when(resultSet.getString(2)).thenReturn("House").thenReturn("Office");
    List<Product> products = ProductConverter.convert(resultSet);
    assertEquals("Office",products.get(1).getProductName());
    resultSet.close();
  }

}
