SELECT *
from seller s
left join (
	SELECT s.id as seller_id, count(p.id) as product_count
	from seller s
	left join product p on p.fk_seller_id = s.id
    group by s.id
) as prod_count on s.id = prod_count.seller_id

WHERE prod_count.product_count in (1, 9)
