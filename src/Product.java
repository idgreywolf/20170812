public class Product {

  private int id;
  private String productName;
  private String productAddress;

  public Product(int id, String productName, String productAddress) {
    this.id = id;
    this.productName = productName;
    this.productAddress = productAddress;
  }

  public int getId() {
    return id;
  }

  public String getProductName() {
    return productName;
  }

  public String getProductAddress() {
    return productAddress;
  }
}
