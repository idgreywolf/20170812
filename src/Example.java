public class Example {

  private int id;

  private String name;

  private String lastName;

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getLastName() {
    return lastName;
  }


  public Example(int id, String name, String lastName) {
    this.id = id;
    this.name = name;
    this.lastName = lastName;
  }


  private Example(){

  }



  public static ExampleBuilder builder() {
    return new ExampleBuilder();
  }


  public static class ExampleBuilder {

    private Example example = new Example();

    public ExampleBuilder id(int id) {
        example.id = id;
        return this;
    }

    public ExampleBuilder name(String name) {
      example.name = name;
      return this;
    }

    public ExampleBuilder lastName(String lastName) {
      example.lastName = lastName;
      return this;
    }

    public Example build() {
      return example;
    }
  }

}
