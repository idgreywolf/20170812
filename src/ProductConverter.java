import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductConverter {

  public static List<Product> convert(ResultSet resultSet) throws SQLException {
    List<Product> products = new ArrayList<>();

    while(resultSet.next()) {
       products.add(convertSingle(resultSet));
     }

    return products;
  }

  private static Product convertSingle(ResultSet resultSet) throws SQLException {
    return new Product(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(4));
  }

}
