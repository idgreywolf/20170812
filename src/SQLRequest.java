import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLRequest {

  private final Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3308/auction", "root","123456");


  public SQLRequest() throws SQLException {
  }


  public ResultSet executeQuery(String sql) {
    try {
      PreparedStatement preparedStatement = connection.prepareStatement(sql);
      return preparedStatement.executeQuery(sql);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }


}
